package com.uzzors2k.blucar;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Very basic unit test, to verify that the commands are generated as expected from the
 * communications module.
 */
public class ExampleUnitTest {

    // These are duplicated from RcCarCommunicator, should move to a single location
    private final static byte SET_MOTOR_POWER = (byte)128;
    private final static byte GET_MOTOR_POWER = (byte)64;
    private final static byte SET_MOTOR_DIRECTION = (byte)129;
    private final static byte GET_MOTOR_DIRECTION = (byte)65;
    private final static byte SET_STEERING_ANGLE = (byte)130;
    private final static byte GET_STEERING_ANGLE = (byte)66;
    private final static byte SET_FRONT_LIGHTS = (byte)131;
    private final static byte GET_FRONT_LIGHTS = (byte)67;
    private final static byte SET_BRAKE_LIGHTS = (byte)132;
    private final static byte GET_BRAKE_LIGHTS = (byte)68;

    @Test
    public void sendCommandIsGeneratedCorrectly() {
        RcCarCommunicator testRcCarCommunicator = new RcCarCommunicator(
                (float)1.0,
                (float)0.0,
                (float)1.0,
                (float)1.0);

        testRcCarCommunicator.setHeadLight(true);
        testRcCarCommunicator.updateSteeringFromAccelerometer((float)-4.5);
        testRcCarCommunicator.setPowerAndDirection(true, true);

        byte[] messageToSend = testRcCarCommunicator.getDataPacketBytesFromSending();
        assertEquals(messageToSend[0], SET_MOTOR_POWER);
        assertEquals(messageToSend[1], (byte)255);
        assertEquals(messageToSend[2], SET_MOTOR_DIRECTION);
        assertEquals(messageToSend[3], (byte)255);
        assertEquals(messageToSend[4], SET_STEERING_ANGLE);
        assertEquals(messageToSend[5], (byte)68);
        assertEquals(messageToSend[6], SET_FRONT_LIGHTS);
        assertEquals(messageToSend[7], (byte)255);
        assertEquals(messageToSend[8], SET_BRAKE_LIGHTS);
        assertEquals(messageToSend[9], (byte)0);
    }

    @Test
    public void getCommandIsGeneratedCorrectly() {
        RcCarCommunicator testRcCarCommunicator = new RcCarCommunicator((float)1.0,
                (float)0.0,
                (float)1.0,
                (float)1.0);

        byte[] messageToSend = testRcCarCommunicator.getDataPacketBytesForStateRequest();
        assertEquals(messageToSend[0], GET_MOTOR_POWER);
        assertEquals(messageToSend[1], GET_MOTOR_DIRECTION);
        assertEquals(messageToSend[2], GET_STEERING_ANGLE);
        assertEquals(messageToSend[3], GET_FRONT_LIGHTS);
        assertEquals(messageToSend[4], GET_BRAKE_LIGHTS);
    }

    @Test
    public void leftSteeringSaturationWorksCorrectly() {
        RcCarCommunicator testRcCarCommunicator = new RcCarCommunicator(
                (float)1.0,
                (float)0.0,
                (float)0.2,
                (float)1.0);

        testRcCarCommunicator.updateSteeringFromAccelerometer((float)7.5);

        byte[] messageToSend = testRcCarCommunicator.getDataPacketBytesFromSending();
        assertEquals(messageToSend[5], (byte)-104);
    }

    @Test
    public void rightSteeringSaturationWorksCorrectly() {
        RcCarCommunicator testRcCarCommunicator = new RcCarCommunicator(
                (float)1.0,
                (float)0.0,
                (float)1.0,
                (float)0.3);

        testRcCarCommunicator.updateSteeringFromAccelerometer((float)-7.5);

        byte[] messageToSend = testRcCarCommunicator.getDataPacketBytesFromSending();
        assertEquals(messageToSend[5], (byte)88);
    }
}