package com.uzzors2k.blucar;

class RcCarCommunicator implements IRcCarCommunicator {

    // RC car state
    private byte motorPower = 0;
    private byte motorDirection = 0;
    private byte steeringAngle = 0;
    private byte headLightIntensity = 0;
    private byte brakeLightIntensity = 0;

    // Communication Protocol
    private final static byte SET_MOTOR_POWER = (byte)128;
    private final static byte GET_MOTOR_POWER = (byte)64;
    private final static byte SET_MOTOR_DIRECTION = (byte)129;
    private final static byte GET_MOTOR_DIRECTION = (byte)65;
    private final static byte SET_STEERING_ANGLE = (byte)130;
    private final static byte GET_STEERING_ANGLE = (byte)66;
    private final static byte SET_FRONT_LIGHTS = (byte)131;
    private final static byte GET_FRONT_LIGHTS = (byte)67;
    private final static byte SET_BRAKE_LIGHTS = (byte)132;
    private final static byte GET_BRAKE_LIGHTS = (byte)68;

    // Internal states
    private float sensitivity;
    private float centerOffset;
    private float leftMostSaturation;
    private float rightMostSaturation;

    RcCarCommunicator(float sensitivity, float centerOffset, float leftMostSaturation,
                           float rightMostSaturation) {
        this.sensitivity = sensitivity;
        this.centerOffset = centerOffset;
        this.leftMostSaturation = leftMostSaturation;
        this.rightMostSaturation = rightMostSaturation;
    }

    @Override
    public void setHeadLight(boolean on) {
        if (on) headLightIntensity = (byte)255;
        else headLightIntensity = 0;
    }

    @Override
    public void setBrakeLight(boolean on) {
        if (on) brakeLightIntensity = (byte)255;
        else brakeLightIntensity = 0;
    }

    @Override
    public void setPowerAndDirection(boolean motorOn, boolean direction) {
        if (motorOn) motorPower = (byte)255;
        else motorPower = 0;

        if (direction) motorDirection = (byte)255;
        else motorDirection = 0;
    }

    @Override
    public void updateSteeringFromAccelerometer(float acceleration) {
        final float gravityConstant = (float)9.81;

        // Ratio from -1.0 to 1.0
        float angleRatio = sensitivity * (acceleration / gravityConstant) + centerOffset;
        if (angleRatio > leftMostSaturation) angleRatio = leftMostSaturation;
        if (angleRatio < -rightMostSaturation) angleRatio = -rightMostSaturation;

        steeringAngle = (byte)(angleRatio * 127 + 127);
    }

    @Override
    public byte[] getDataPacketBytesFromSending() {
        byte[] dataPacket = new byte[10];

        dataPacket[0] = SET_MOTOR_POWER;
        dataPacket[1] = motorPower;
        dataPacket[2] = SET_MOTOR_DIRECTION;
        dataPacket[3] = motorDirection;
        dataPacket[4] = SET_STEERING_ANGLE;
        dataPacket[5] = steeringAngle;
        dataPacket[6] = SET_FRONT_LIGHTS;
        dataPacket[7] = headLightIntensity;
        dataPacket[8] = SET_BRAKE_LIGHTS;
        dataPacket[9] = brakeLightIntensity;

        return dataPacket;
    }

    @Override
    public byte[] getDataPacketBytesForStateRequest() {
        byte[] dataPacket = new byte[10];

        dataPacket[0] = GET_MOTOR_POWER;
        dataPacket[1] = GET_MOTOR_DIRECTION;
        dataPacket[2] = GET_STEERING_ANGLE;
        dataPacket[3] = GET_FRONT_LIGHTS;
        dataPacket[4] = GET_BRAKE_LIGHTS;

        return dataPacket;
    }
}
