package com.uzzors2k.blucar;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

class PreferencesManager {
    private static final String MY_PREFS_NAME = "BluCarPrefs";
    private static final String PREF_SENSITIVITY = "Sensitivity";
    private static final String PREF_CENTER_OFFSET = "CenterOffset";
    private static final String PREF_LEGACY_MODE = "LegacyMode";
    private static final String PREF_REVERSE_MOTOR = "ReverseMotor";
    private static final String PREF_REVERSE_SERVO = "ReverseServo";
    private static final String PREF_LEFT_SAT = "LeftMostSaturation";
    private static final String PREF_RIGHT_SAT = "RightMostSaturation";

    float sensitivity = (float)1.0;
    float centerOffset = (float)0.0;
    float leftMostSaturation = (float)1.0;
    float rightMostSaturation = (float)1.0;
    boolean legacyMode = false;
    boolean reverseMotor = false;
    boolean reverseServo = false;

    private SharedPreferences pref;

    PreferencesManager(Context baseContext) {
        pref = baseContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        loadSettings();
    }

    void loadSettings() {
        sensitivity = pref.getFloat(PREF_SENSITIVITY, (float)1.0);
        centerOffset = pref.getFloat(PREF_CENTER_OFFSET, (float)0.0);
        leftMostSaturation = pref.getFloat(PREF_LEFT_SAT, (float)1.0);
        rightMostSaturation = pref.getFloat(PREF_RIGHT_SAT, (float)1.0);
        legacyMode = pref.getBoolean(PREF_LEGACY_MODE, false);
        reverseMotor = pref.getBoolean(PREF_REVERSE_MOTOR, false);
        reverseServo = pref.getBoolean(PREF_REVERSE_SERVO, false);
    }

    void saveSettings() {
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(PREF_SENSITIVITY, sensitivity);
        editor.putFloat(PREF_CENTER_OFFSET, centerOffset);
        editor.putFloat(PREF_LEFT_SAT, leftMostSaturation);
        editor.putFloat(PREF_RIGHT_SAT, rightMostSaturation);
        editor.putBoolean(PREF_LEGACY_MODE, legacyMode);
        editor.putBoolean(PREF_REVERSE_MOTOR, reverseMotor);
        editor.putBoolean(PREF_REVERSE_SERVO, reverseServo);
        editor.apply();
    }
}
