package com.uzzors2k.blucar;

interface IRcCarCommunicator {
    void setHeadLight(boolean on);
    void setBrakeLight(boolean on);
    void setPowerAndDirection(boolean motorOn, boolean direction);
    void updateSteeringFromAccelerometer(float acceleration);
    byte[] getDataPacketBytesFromSending();
    byte[] getDataPacketBytesForStateRequest();
}
