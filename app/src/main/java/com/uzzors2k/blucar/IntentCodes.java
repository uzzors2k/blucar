package com.uzzors2k.blucar;

enum IntentCodes {
    REQUEST_CONNECT_DEVICE,
    REQUEST_ENABLE_BT,
    SETTINGS_COMPLETE
}
