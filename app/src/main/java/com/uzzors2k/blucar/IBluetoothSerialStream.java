package com.uzzors2k.blucar;

interface IBluetoothSerialStream {
	void updateConnectionIndicatorState(boolean state);
	void newStreamDataReceived(String data);
}
