package com.uzzors2k.blucar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IBluetoothSerialStream {

    // Sensor object used to handle accelerometer
    private SensorManager mySensorManager;
    private Sensor accSensor;
    private long lastWrite = 0;

    // GUI Objects
    private TextView textViewAcceleration;
    private TextView textViewReceivedData;
    private Button connectButton;
    boolean ledStat = false;

    // Business Logic
    private BluetoothConnectionWizard mBluetoothConnectionWizard;
    private IRcCarCommunicator mRcCarCommunicator;
    private PreferencesManager mPreferencesManager;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get handles to the layout elements
        final Button ledButton = findViewById(R.id.buttonHeadLights);
        final Button forwardButton = findViewById(R.id.buttonForward);
        final Button reverseButton = findViewById(R.id.buttonReverse);
        connectButton = findViewById(R.id.buttonConnect);
        textViewAcceleration = findViewById(R.id.textViewAcceleration);
        textViewReceivedData = findViewById(R.id.textViewReceivedData);
        textViewAcceleration.setText(R.string.textView_acceleration);
        textViewReceivedData.setText(R.string.textView_receivedData);

        // Set Sensor
        mySensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors = mySensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if (sensors.size() > 0) {
            accSensor = sensors.get(0);
        }

        // Create business logic objects
        mBluetoothConnectionWizard = new BluetoothConnectionWizard(this, this);
        mBluetoothConnectionWizard.addListener(this);
        mPreferencesManager = new PreferencesManager(this);
        updateCommunicatorFromSettings();

        // Button click event handlers
        connectButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mBluetoothConnectionWizard.getConnectionState()) {
                    mBluetoothConnectionWizard.disconnect();
                } else {
                    mBluetoothConnectionWizard.connect();
                }
            }
        });

        ledButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ledStat) {
                    mRcCarCommunicator.setHeadLight(false);
                    ledButton.setText(R.string.button_headlights_on);
                    ledStat = false;
                } else {
                    mRcCarCommunicator.setHeadLight(true);
                    ledButton.setText(R.string.button_headlights_off);
                    ledStat = true;
                }
            }
        });

        forwardButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        mRcCarCommunicator.setPowerAndDirection(true, !mPreferencesManager.reverseMotor);
                        return true;
                    case MotionEvent.ACTION_UP:
                        mRcCarCommunicator.setPowerAndDirection(false, false);
                        return true;
                }
                return false;
            }
        });

        reverseButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        mRcCarCommunicator.setPowerAndDirection(true, mPreferencesManager.reverseMotor);
                        return true;
                    case MotionEvent.ACTION_UP:
                        mRcCarCommunicator.setPowerAndDirection(false, false);
                        return true;
                }
                return false;
            }
        });
    }

    /**
     * Receive intent results for activities which have been launched in this Activity's name
     *
     * @param requestCode - Intent identification code
     * @param resultCode - Result of the intent
     * @param data - Extra data sent with the intent result
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IntentCodes.SETTINGS_COMPLETE.ordinal()) {
            updateCommunicatorFromSettings();
        } else {
            mBluetoothConnectionWizard.processActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent, IntentCodes.SETTINGS_COMPLETE.ordinal());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private final SensorEventListener mSensorListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            // Sensor change events fire all the time, and serve as the back-bone in the
            // communications update.
            final int MOVE_TIME_MS = 100;

            long date = System.currentTimeMillis();
            if (date - lastWrite > MOVE_TIME_MS) {

                float accelerationY = event.values[0];
                if (mPreferencesManager.reverseServo)
                {
                    accelerationY *= -1.0f;
                }

                // Testing, use the seekbar value as the acceleration
                //accelerationY = 9.81f * (((float)testSeekBar.getProgress() / 50.0f) - 1.0f);

                // Update the accelerometer text
                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits(1);
                textViewAcceleration.setText(String.format("%s: %s",
                        getString(R.string.textView_acceleration), df.format(accelerationY)));

                // Update the steering command, and send over bluetooth
                mRcCarCommunicator.updateSteeringFromAccelerometer(accelerationY);
                mBluetoothConnectionWizard.write(mRcCarCommunicator.getDataPacketBytesFromSending());

                lastWrite = date;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mySensorManager.registerListener(mSensorListener, accSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onDestroy() {
        mySensorManager.unregisterListener(mSensorListener);
        mBluetoothConnectionWizard.emptyOutStream();
        mBluetoothConnectionWizard.disconnect();
        super.onDestroy();
    }

    @Override
    public void updateConnectionIndicatorState(boolean state) {
        if (state) {
            connectButton.setText(R.string.button_connect_disconnect);
        } else {
            connectButton.setText(R.string.button_connect_start);
        }
    }

    @Override
    public void newStreamDataReceived(String data) {
        // Data received from RC car
        textViewReceivedData.setText(String.format("%s: %s", getString(R.string.textView_receivedData), data));
    }

    private void updateCommunicatorFromSettings() {
        mPreferencesManager.loadSettings();
        if (mPreferencesManager.legacyMode) {
            Toast.makeText(this, R.string.textView_legacyMode, Toast.LENGTH_SHORT).show();
            mRcCarCommunicator = new LegacyRcCarCommunicator(
                    mPreferencesManager.sensitivity,
                    mPreferencesManager.centerOffset);
        } else {
            mRcCarCommunicator = new RcCarCommunicator(
                    mPreferencesManager.sensitivity,
                    mPreferencesManager.centerOffset,
                    mPreferencesManager.leftMostSaturation,
                    mPreferencesManager.rightMostSaturation
            );
        }
    }
}
