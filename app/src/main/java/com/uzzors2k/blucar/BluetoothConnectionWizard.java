package com.uzzors2k.blucar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

class BluetoothConnectionWizard {

    // Bluetooth Stuff
    private BluetoothAdapter mBluetoothAdapter;
    private OutputStream outStream = null;
    private InputStream inStream = null;
    // Well known SPP UUID (will *probably* map to RFCOMM channel 1 (default) if not in use);
    private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private String deviceName = null;

    private boolean connectStat = false;

    private Toast failToast;
    private Toast noBTtoast;
    private Toast socketToast;
    private Toast noRFCOMtoast;
    private Toast noStreamToast;

    private ProgressDialog myProgressDialog;

    private Handler mHandler;
    private Handler mReadThreadHandler;

    private Context baseContext;
    private Activity baseActivity;

    private List<IBluetoothSerialStream> listeners = new ArrayList<>();

    /**
     * Create new bluetooth connection wizard class. Takes care of connecting to a BT module,
     * opening an RFCOM port, setting IO streams, and updating listeners of new events.
     * @param context Base application context
     * @param activity Base activity the wizard is operated from
     */
    BluetoothConnectionWizard(Context context, Activity activity) {
        baseContext = context;
        baseActivity = activity;

        failToast = Toast.makeText(baseActivity, R.string.failedToConnect, Toast.LENGTH_SHORT);
        noBTtoast = Toast.makeText(baseActivity, R.string.noBTstring, Toast.LENGTH_SHORT);
        socketToast = Toast.makeText(baseActivity, R.string.noSocketString, Toast.LENGTH_SHORT);
        noRFCOMtoast = Toast.makeText(baseActivity, R.string.noRFcomString, Toast.LENGTH_SHORT);
        noStreamToast = Toast.makeText(baseActivity, R.string.noIOstream, Toast.LENGTH_SHORT);

        myProgressDialog = new ProgressDialog(activity);

        // Handler which waits on the connection status result
        mHandler = new ConnectionHandler(this);

        // Handler which consumes read events from the iostream
        mReadThreadHandler = new ReadThreadUpdatenHandler(this);

        // Check whether bluetooth adapter exists
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(context, R.string.no_bt_device, Toast.LENGTH_LONG).show();
            ((Activity) baseContext).finish();
            return;
        }

        // If BT is not on, request that it be enabled.
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            baseActivity.startActivityForResult(enableIntent, IntentCodes.REQUEST_ENABLE_BT.ordinal());
        }
    }

    /**
     * Thread used to connect to a specified Bluetooth Device
     */
    private class ConnectThread extends Thread {
        private String address;
        private int errorCode;

        final static int NO_ERRORS = 0;
        final static int NO_BT_DEVICE = 1;
        final static int NO_RFCOMM = 2;
        final static int ERROR_SOCKET = 4;
        final static int NO_STREAM = 8;

        ConnectThread(String MACaddress) {
            address = MACaddress;
            errorCode = NO_ERRORS;
        }

        public void run() {
            // When this returns, it will 'know' about the server,
            // via it's MAC address.
            try {
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

                // We need two things before we can successfully connect
                // (authentication issues aside): a MAC address, which we
                // already have, and an RFCOMM channel.
                // Because RFCOMM channels (aka ports) are limited in
                // number, Android doesn't allow you to use them directly;
                // instead you request a RFCOMM mapping based on a service
                // ID. In our case, we will use the well-known SPP Service
                // ID. This ID is in UUID (GUID to you Microsofties)
                // format. Given the UUID, Android will handle the
                // mapping for you. Generally, this will return RFCOMM 1,
                // but not always; it depends what other BlueTooth services
                // are in use on your Android device.

                try {
                    BluetoothSocket btSocket = device.createRfcommSocketToServiceRecord(SPP_UUID);

                    // Discovery may be going on, e.g., if you're running a
                    // 'scan for devices' search from your handset's Bluetooth
                    // settings, so we call cancelDiscovery(). It doesn't hurt
                    // to call it, but it might hurt not to... discovery is a
                    // heavyweight process; you don't want it in progress when
                    // a connection attempt is made.
                    mBluetoothAdapter.cancelDiscovery();

                    // Blocking connect, for a simple client nothing else can
                    // happen until a successful connection is made, so we
                    // don't care if it blocks.
                    try {
                        btSocket.connect();


                        // Create a data stream so we can talk to server.
                        try {
                            outStream = btSocket.getOutputStream();
                        } catch (IOException e2) {
                            errorCode += NO_STREAM;
                        }

                        // Create input stream
                        try {
                            inStream = btSocket.getInputStream();
                        } catch (IOException e) {
                            errorCode += NO_STREAM;
                        }


                    } catch (IOException e1) {
                        errorCode += ERROR_SOCKET;
                        //Log.d("BluGeiger", "Error opening socket: " + e1);
                        try {
                            btSocket.close();
                        } catch (IOException e2) {
                            //Log.d("BluGeiger", "Error closing socket: " + e2);
                        }
                    }


                } catch (IOException e) {
                    errorCode += NO_RFCOMM;
                    //Log.d("BluGeiger", "Couldn't create RFCOMM socket." + e);
                }

            } catch (IllegalArgumentException e) {
                errorCode += NO_BT_DEVICE;
                //Log.d("BluGeiger", "Couldn't get bluetooth device." + e);
            }

            // Send final result
            Message mMessageData = Message.obtain();
            mMessageData.obj = errorCode;
            mHandler.sendMessage(mMessageData);
        }
    }

    void processActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IntentCodes.REQUEST_CONNECT_DEVICE.ordinal()) {
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Show please wait dialog
                myProgressDialog = ProgressDialog.show(baseActivity, baseActivity.getResources().getString(R.string.pleaseWait), baseActivity.getResources().getString(R.string.makingConnectionString), true);

                // Get the device MAC address
                String deviceAddress;
                try {
                    deviceAddress = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                } catch (NullPointerException e) {
                    deviceAddress = baseContext.getString(R.string.unknownName);
                }

                // Get the device name, for status bar
                try {
                    deviceName = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_NAME);
                } catch (NullPointerException e) {
                    deviceName = baseContext.getString(R.string.unknownName);
                }

                // Connect to device with specified MAC address
                ConnectThread mConnectThread = new ConnectThread(deviceAddress);
                mConnectThread.start();

            } else {
                // Failure retrieving MAC address
                Toast.makeText(baseContext, R.string.macFailed, Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == IntentCodes.REQUEST_ENABLE_BT.ordinal()) {
            // When the request to enable Bluetooth returns
            if (resultCode != Activity.RESULT_OK) {
                // User did not enable Bluetooth or an error occured
                Toast.makeText(baseContext, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                baseActivity.finish();
            }
        }
    }


    /**
     * Class which takes care of message handling for connection status
     *
     * @author det
     */
    private static class ConnectionHandler extends Handler {
        private final WeakReference<BluetoothConnectionWizard> mService;

        ConnectionHandler(BluetoothConnectionWizard service) {
            mService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            BluetoothConnectionWizard service = mService.get();
            if (service != null) {
                service.handleMessage(msg);
            }
        }
    }

    /**
     * The actual message handling method for checking how the connection went
     *
     * @param msg - Contains error code information?
     */
    private void handleMessage(Message msg) {
        if (myProgressDialog.isShowing()) {
            myProgressDialog.dismiss();
        }

        // Check if bluetooth connection was made to selected device
        int errorCode = (Integer) msg.obj;

        //Log.d("BluGeiger", "Device error code: " + errorCode);

        switch (errorCode) {

            case ConnectThread.NO_ERRORS: {
                // No errors during connection, so fire up as usual

                // Set button to display current status
                connectStat = true;
                updateConnectionIndicatorState(true);

                // Create the reading thread
                ReadThread mReadThread = new ReadThread(inStream);
                mReadThread.start();
            }
            break;

            case ConnectThread.NO_BT_DEVICE: {
                // Device does not have bluetooth
                noBTtoast.show();

                // Set button to display current status
                connectStat = false;
                updateConnectionIndicatorState(false);
            }
            break;

            case ConnectThread.ERROR_SOCKET: {
                // Could not create a socket
                socketToast.show();

                // Set button to display current status
                connectStat = false;
                updateConnectionIndicatorState(false);
            }
            break;

            case ConnectThread.NO_RFCOMM: {
                // Unable to create RFCOMM socket
                noRFCOMtoast.show();

                // Set button to display current status
                connectStat = false;
                updateConnectionIndicatorState(false);
            }
            break;

            case ConnectThread.NO_STREAM: {
                // Not able to open IO stream
                noStreamToast.show();

                // Set button to display current status
                connectStat = false;
                updateConnectionIndicatorState(false);
            }
            break;

            default: {
                // Connection failed
                failToast.show();

                // Set button to display current status
                connectStat = false;
                updateConnectionIndicatorState(false);
            }

        }
    }


    /**
     * Thread used to read from the input stream
     */
    private class ReadThread extends Thread {
        private InputStream inputStream;

        ReadThread(InputStream stream) {
            inputStream = stream;
        }

        public void run() {
            // Read from ioStream
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            Message mMessageData;
            try {
                while ((line = r.readLine()) != null) {

                    // Put data into a message and send to the handler
                    mMessageData = Message.obtain();
                    mMessageData.obj = line;
                    mReadThreadHandler.sendMessage(mMessageData);

                    //Log.d("BluGeiger", "Received: " + line);
                }
            } catch (IOException e) {
                //Log.d("BluGeiger", "Error reading: " + e);
            }
        }
    }

    /**
     * Class which takes care of message handling for the Inputstream reader
     *
     * @author det
     */
    private static class ReadThreadUpdatenHandler extends Handler {
        private final WeakReference<BluetoothConnectionWizard> mService;

        ReadThreadUpdatenHandler(BluetoothConnectionWizard service) {
            mService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            BluetoothConnectionWizard service = mService.get();
            if (service != null) {
                service.handleReadThreadMessage(msg);
            }
        }
    }

    /**
     * The actual message handling method for received messages from the input stream
     *
     * @param msg - Input stream data, from bluetooth unit?
     */
    private void handleReadThreadMessage(Message msg) {
        // Update the textview with the message
        updateListenersWithReceivedData((String) msg.obj);

    }

    void write(byte[] bs) {
        if (outStream != null) {
            try {
                outStream.write(bs);
            } catch (IOException e) {
                //Log.d("BluGeiger", "IO error when wrting to buffer." + e);
            }
        }
    }

    void writeString(String data) {
        // Write a string to the outStream
        write(data.getBytes());
    }

    void emptyOutStream() {
        if (outStream != null) {
            try {
                outStream.flush();
            } catch (IOException ignored) {
            }
        }
    }

    void connect() {
        // Launch the DeviceListActivity to see devices and do scan
        Intent serverIntent = new Intent(baseActivity, DeviceListActivity.class);
        baseActivity.startActivityForResult(serverIntent, IntentCodes.REQUEST_CONNECT_DEVICE.ordinal());
    }

    /**
     * Close IO streams and close the socket
     */
    void disconnect() {
        if (outStream != null) {
            try {
                outStream.close();
                inStream.close();
                connectStat = false;
                updateConnectionIndicatorState(false);
            } catch (IOException ignored) {
            }
        }
    }

    /**
     * Returns true when connected to a bluetooth device.
     *
     * @return connectStat
     */
    boolean getConnectionState() {
        return connectStat;
    }

    /**
     * Adds the passed object to the list of listening objects
     *
     * @param listener - Listener of bluetooth connection events
     */
    void addListener(IBluetoothSerialStream listener) {
        listeners.add(listener);
    }

    /**
     * Alert all listeners of the event
     */
    private void updateConnectionIndicatorState(boolean state) {
        for (IBluetoothSerialStream listener : listeners) {
            listener.updateConnectionIndicatorState(state);
        }
    }

    /**
     * Alert all listening objects with new data string
     *
     * @param data - Data to update listeners with
     */
    private void updateListenersWithReceivedData(String data) {
        for (IBluetoothSerialStream listener : listeners) {
            listener.newStreamDataReceived(data);
        }
    }

    String getConnectedDeviceName() {
        return deviceName;
    }
}
