package com.uzzors2k.blucar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.text.DecimalFormat;

public class SettingsActivity extends AppCompatActivity {

    private PreferencesManager mPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Get handles to the GUI elements
        final TextView sensitivityTextView = findViewById(R.id.textView_sensitivity);
        final SeekBar sensitivitySeekBar = findViewById(R.id.seekBar_sensitivity);

        final TextView centerOffsetTextView = findViewById(R.id.textView_centerOffset);
        final SeekBar centerOffsetSeekBar = findViewById(R.id.seekBar_centerOffset);

        final TextView leftMostSatTextView = findViewById(R.id.textView_leftMax);
        final SeekBar leftMostSatSeekBar = findViewById(R.id.seekBar_leftMax);

        final TextView rightMostSatTextView = findViewById(R.id.textView_rightMax);
        final SeekBar rightMostSatSeekBar = findViewById(R.id.seekBar_rightMax);

        final Switch legacyModeSwitch = findViewById(R.id.switch_legacyMode);
        final Switch reverseMotorSwitch = findViewById(R.id.switch_reverseMotor);
        final Switch reverseServoSwitch = findViewById(R.id.switch_reverseServo);

        // Seek bar event handlers
        sensitivitySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // From 0.0 to +2.0
                mPreferencesManager.sensitivity = (float)progress / 50.0f;
                updateLabeledFloatDisplay(sensitivityTextView,
                        R.string.textView_sensitivity,
                        mPreferencesManager.sensitivity);
            }
        });

        centerOffsetSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // From -1.0 to +1.0
                mPreferencesManager.centerOffset = ((float)progress / 50.0f) - 1.0f;
                updateLabeledFloatDisplay(centerOffsetTextView,
                        R.string.textView_centerOffset,
                        mPreferencesManager.centerOffset);
            }
        });

        leftMostSatSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // From 0 to +1.0
                mPreferencesManager.leftMostSaturation = (float)progress / 100.0f;
                updateLabeledFloatDisplay(leftMostSatTextView,
                        R.string.textView_leftMax,
                        mPreferencesManager.leftMostSaturation);
            }
        });

        rightMostSatSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // From 0 to +1.0
                mPreferencesManager.rightMostSaturation = (float)progress / 100.0f;
                updateLabeledFloatDisplay(rightMostSatTextView,
                        R.string.textView_rightMax,
                        mPreferencesManager.rightMostSaturation);
            }
        });

        // Switch handler
        legacyModeSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPreferencesManager.legacyMode = isChecked;
                centerOffsetSeekBar.setEnabled(!isChecked);
                sensitivitySeekBar.setEnabled(!isChecked);
                leftMostSatSeekBar.setEnabled(!isChecked);
                rightMostSatSeekBar.setEnabled(!isChecked);
            }
        });

        reverseMotorSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPreferencesManager.reverseMotor = isChecked;
            }
        });

        reverseServoSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPreferencesManager.reverseServo = isChecked;
            }
        });

        // Set the GUI elements to the right values once the handlers are ready
        mPreferencesManager = new PreferencesManager(getBaseContext());
        sensitivitySeekBar.setProgress((int)(mPreferencesManager.sensitivity * 50));
        centerOffsetSeekBar.setProgress((int)(((mPreferencesManager.centerOffset + (float)1.0)/(float)2.0)*100));
        leftMostSatSeekBar.setProgress((int)(mPreferencesManager.leftMostSaturation * 100));
        rightMostSatSeekBar.setProgress((int)(mPreferencesManager.rightMostSaturation * 100));
        legacyModeSwitch.setChecked(mPreferencesManager.legacyMode);
        reverseMotorSwitch.setChecked(mPreferencesManager.reverseMotor);
        reverseServoSwitch.setChecked(mPreferencesManager.reverseServo);
    }

    @Override
    protected void onPause() {
        mPreferencesManager.saveSettings();
        super.onPause();
    }

    private void updateLabeledFloatDisplay(TextView view, int labelTextResourceId, float number) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        view.setText(String.format("%s: %s",
                getString(labelTextResourceId),
                df.format(number)));
    }
}
