package com.uzzors2k.blucar;

class LegacyRcCarCommunicator implements IRcCarCommunicator {

    // RC car state
    private boolean motorPower = false;
    private boolean motorDirection = false;
    private boolean headLightIntensity = false;
    private boolean brakeLightIntensity = false;
    private enum SteeringAngles{Left, Center, Right}
    private SteeringAngles steeringAngle = SteeringAngles.Center;

    // Internal states
    private float sensitivity;
    private float centerOffset;

    LegacyRcCarCommunicator(float sensitivity, float centerOffset) {
        this.sensitivity = sensitivity;
        this.centerOffset = centerOffset;
    }

    @Override
    public void setHeadLight(boolean on) {
        headLightIntensity = on;
    }

    @Override
    public void setBrakeLight(boolean on) {
        brakeLightIntensity = on;
    }

    @Override
    public void setPowerAndDirection(boolean motorOn, boolean direction) {
        motorPower = motorOn;
        motorDirection = direction;
    }

    @Override
    public void updateSteeringFromAccelerometer(float acceleration) {
        final float gravityConstant = (float)9.81;

        // Ratio from -1.0 to 1.0
        float angleRatio = sensitivity * (acceleration / gravityConstant) + centerOffset;
        if (angleRatio > 1.0) angleRatio = (float)1.0;
        if (angleRatio < -1.0) angleRatio = (float)-1.0;

        if (angleRatio < -0.5) {
            steeringAngle = SteeringAngles.Left;
        } else if (angleRatio > 0.5) {
            steeringAngle = SteeringAngles.Right;
        } else {
            steeringAngle = SteeringAngles.Center;
        }
    }

    @Override
    public byte[] getDataPacketBytesFromSending() {
        byte[] dataPacket = new byte[1];

        byte bit7 = headLightIntensity ? (byte) 1 : (byte) 0;
        byte bit6 = brakeLightIntensity ? (byte) 1 : (byte) 0;
        byte bit5 = (motorPower && !motorDirection) ? (byte) 1 : (byte) 0;
        byte bit4 = (motorPower && motorDirection) ? (byte) 1 : (byte) 0;
        byte bit3 = (steeringAngle == SteeringAngles.Left) ? (byte) 1 : (byte) 0;
        byte bit2 = (steeringAngle == SteeringAngles.Right) ? (byte) 1 : (byte) 0;
        byte bit1 = (byte) 0;
        byte bit0 = (byte) 0;

        dataPacket[0] = (byte)((bit7 << 7)|(bit6 << 6)|(bit5 << 5)|(bit4 << 4)|(bit3 << 3)|
                (bit2 << 2)|(bit1 << 1)|(bit0));
        return dataPacket;
    }

    @Override
    public byte[] getDataPacketBytesForStateRequest() {
        // No data request command implemented
        return new byte[0];
    }
}
